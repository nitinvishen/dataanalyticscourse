The Leela,is an opulent luxury hotel but the prices are very reasonable. I was treated like royalty from the moment you drive up and enter. The rooms are a dream,bathrooms exquisite,room service ,great food,very quick,staff impeccable. The dining options are varied. Excellent security,cars are stopped and trunk and hood checked out, and at the time I was there there was a metal detector,there was a parlimentary convention there at the time. It is an oasis in the diplomatic enclave, with nice views. Never have stayed in a hotel so fabulous,had lunch at the Oberoi in Agra and I feel the Leela is much nicer.

Having lived in India for 4 years, my family and I have definitely visited many of the finer hotels of New Delhi, but nothing has reached the high standard set by The Leela. It has been an oasis for us throughout our entire stay and we really got to enjoy all of the facilities when we stayed there for more than a week. 

The staff is nothing but incredibly service-minded and friendly. Not once did we get the feel that this was in a self-serving interest (i.e. that they were fishing for gratuities). This entailed everyone from the bell-boys, waiters and receptionists to the more general cleaners, guards and door-openers. Much of our enjoyment must be credited to the staff. They all went the extra mile to make us feel welcome and at home, which we absolutely did. 

The entire hotel is exceptionally clean - something we appreciate immensely. If there was anything we requested they would always provide us with it, with a smile. 

The facilities are also of the best quality and standards. I visited the gym several times and appreciated the variety of equipment, as well as the privacy I was given. Water and towels are available at the gym, in addition to apples and headsets compatible with the treadmills. The pool is also outstanding; we were served ice-cold water in a picnic basket, lemonades and cold washcloths at no extra expense every time. The pool itself is also very refreshing and it was amazing to escape into the "cold" water after sunbathing in the nearly intolerable heat. 

The restaurants serve food of superb quality. We really like the atmosphere of The Qube with its glass walls and, moreover, it serves a broad range of cuisines. Megu is, however, probably our favorite, merely because we love Japanese food. We highly recommend trying out the restaurants at the hotel, not only because the food is delicious, but also because you're guaranteed to not become sick. 

We had one of our best hotels stay here and we are grateful to The Leela for giving us this wonderful experience before we left.

The service is impeccable from the front door where a warm welcome awaits guests to the butler service assigned to each room. 

The rooms are comfortable. The layered lighting makes it easy to adjust to the task and mood. The temperature is controlled in the room, but only could adjust to a low of 20c (68f). Plenty of bottled water is provided along with fruit and cookies each day. Electrical outlets are by the beds and they except all manner of plugs. 

Bathroom is beautifully appointed with double sinks, a separate tub with a television and separate standing shower with overhead and handheld shower heads. Lighting is okay in the bathroom - a bit yellow for makeup, but there is a lighted mirror which helps.

WiFi is free with a charge for faster service. What is nice is that they offer several price points where you can buy higher speed access for one hour, two hours, a day etc....The high speed works very well. The free wi-fi was very slow.

There are several restaurants on property including Le Cirque Signature, which offers Italian cuisine (rather than French like its sister establishment in NYC). It is an open kitchen on the 10th floor with many views of the city. There is a wine bar in front and the possibility of sitting outside on a gorgeous terrace. The Qube offers all day dining including a robust breakfast buffet with eggs to order, fruit - including whole or cut up, cheese, various breads, meats, traditional Indian dishes and much more. In the evening they offer a menu to appeal to various international appetites.

Also worth enjoying is the spa. It includes a fitness center which is probably one of the cleanest hotel fitness rooms I have ever experienced. The spa is well appointed and one starts to relax just by entering the calm facility.

Dry-cleaning service is available 24 hours with an in by 10:00 back by evening service - with express available. 

There is one shop which is small, so the selection is beautiful, but limited - and mostly focused on women. There a shirts and cufflinks for men.

Overall, what really makes this hotel standout - besides the opulence - is the service. Every turn there is someone to help and make certain that you have everything you need during your stay. Would highly recommend.

